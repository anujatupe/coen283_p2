
/*
	Author - Anuja Tupe
	ID - 1100227
	Programming Assignment 2 - Buddy Memory Allocation System
	Email ID - atupe@scu.edu
*/


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/*
	All constants to be used for arrays and other stuff
*/

#define LINE_LENGTH 300
#define WORD_LENGTH 50
#define TRUE 1
#define FALSE 0

/* 
	Struct definition for the node which will be used in the binary tree of buddy system 
	node_size - the size od the memory in KB
	left - pointer to the left child of that node
	right - pointer to the right child of that node 
*/

typedef struct node {
	unsigned int node_size;
	int is_being_used;
	struct node *left;
	struct node *right;
} node;

/* 
	Global Variables 
*/

int is_memorysize_command_used = FALSE;

/* 
	Function declarations 
*/

	void read_file_contents(char *filename);
	char *trimwhitespace(char *str);
	void execute_memory_size(char line[LINE_LENGTH]);
	unsigned int convert_memory_to_kb(char *memory_in_string);
	node* create_a_new_node(unsigned int node_size);
	node* insert_node(node *root_node, unsigned int new_node_size);
	void memory_size(unsigned int memory_in_kb);
	int merge_nodes(node *parent_node);
	int mark_as_not_used(node *some_node);



/* 
	Main function where execution begins 
*/

	int main() {
		char *filename = malloc (100);  
		printf("Enter the filename which has all memory allocation commands. For eg - t10.dat :");
		scanf("%s", filename);
		read_file_contents(filename);
		return 0;
	}


/* 
	Function to read the memory allocation commands in the input files 
*/

	void read_file_contents(char *filename) {

		printf("\nExecuting the memory commands from file : %s \n", filename);
		char *pos;
		char *first_token;
		int if_st_len = 0;
		FILE *file_pointer;
		if((pos= strchr(filename, '\n')) != NULL) {
			*pos = '\0';
		}
		file_pointer = fopen(filename, "r");

		if(file_pointer != NULL) {
			char line[LINE_LENGTH];
			char copied_line[LINE_LENGTH];
			while(fgets(line, sizeof(line), file_pointer) != NULL) {
				printf("\n\n***********************NEW MEMORY COMMAND**********************");
				strcpy(copied_line, "\0");
				strcpy(copied_line, line);
				printf("\n\nLINE IS: %s", line);
				trimwhitespace(copied_line);
				if((strlen(copied_line) != 1) && (strcmp(copied_line, "\n") != 0)) {
					if(strncmp(copied_line, "memorySize", 10) == 0) {
						printf("\nMemorysize is found");
						execute_memory_size(line);
					} else if (strncmp(copied_line, "slabSize", 8) == 0) {
						printf("\nSlabsize is found");
					} else if (strncmp(copied_line, "alloc", 5) == 0) {
						printf("\nAlloc is found");
					} else if (strncmp(copied_line, "realloc", 6) == 0) {
						printf("\nRealloc is found");
					} else if (strncmp(copied_line, "free", 4) == 0) {
						printf("\nFree is found");
					} else if (strncmp(copied_line, "dump", 4) == 0) {
						printf("\nDump is found");
					} else {
						printf("\nIllegal command");
					}
				}
			}
			fclose(file_pointer);
		} else {
			printf("\nFile was not found");
			exit(1);
		}
	}

/*
	Function to trim the leading and trailing whitespaces from the token which is passed to the function.
	Return value is the string with the whitespaces trimmed from the beginning and the end of the string.
*/

	char *trimwhitespace(char *str) {
		char *end;
		while(isspace(*str)) {
			str++;
		}
		if(*str == 0) {
			return str;
		}
		end = str + strlen(str) - 1;
		while(end > str && isspace(*end)) {
			end--;
		}
		*(end+1) = 0;
		return str;
	}

/* 
	Function to check if the memory size command is in a proper format 
*/

	void execute_memory_size(char line[LINE_LENGTH]) {
		char *token;
		char command[WORD_LENGTH];
		char memory_in_string[WORD_LENGTH];
		unsigned int memory_in_kb;
		token = strtok(line, "(, );\n");
		trimwhitespace(token);
		strcpy(command, token);
		printf("\nToken is a command: %s", token);
		token = strtok(NULL, "(, );\n");
		trimwhitespace(token);
		strcpy(memory_in_string, token);
		printf("\nToken is the memory: %s", token);
		token = strtok(NULL, "(, );\n");
		//trimwhitespace(token);
		if (token != NULL) {
			printf("\nCommand is not in the right format");
			exit(1);
		} else {
			memory_in_kb = convert_memory_to_kb(memory_in_string);
			printf("\nMemory in KB is : %d", memory_in_kb);
			memory_size(memory_in_kb);
		}
	}

/* 
	Function to convert the string memory (GB, MB, KB) to KB in int.
	This function splits the memory size and the memory unit being used.
	Depending on the memory unit being used, the memory size is converted to KB.
	Return value is Memory size in KB.
*/

	unsigned int convert_memory_to_kb(char *memory_in_string) {
		char *memory_unit;
		unsigned int mem_size = 0;
		mem_size = strtod (memory_in_string, &memory_unit);
		printf("\nMemory size is %d", mem_size);
		printf("\nMemory unit is: %s", memory_unit);
		if(strcmp(memory_unit, "GB") == 0) {
			mem_size = mem_size * 1024 * 1024;
		} else if (strcmp(memory_unit, "MB") == 0) {
			mem_size = mem_size * 1024;
		} 
		return mem_size;
	}

/* 
	Function to create a new node which is to be inserted in the buddy system binary tree.
	Returns the newly created node.
*/

	node* create_a_new_node(unsigned int node_size) {
		node *new_node = (node*)malloc(sizeof(node));
		new_node->node_size = node_size;
		new_node->is_being_used = FALSE;
		new_node->left = NULL;
		new_node->right = NULL;
		return new_node;
	}

/* 
	Function to insert newly created node to the buddy system binary tree 
	Returns the root node of the tree.
*/

	node* insert_node(node *root_node, unsigned int new_node_size) {
		if (root_node == NULL) {
			return(create_a_new_node(new_node_size));
		} 
		return NULL;

	}

/* 
	Function to define the base size of the tree. 
	The base size will be used by the root node of the binary tree if the buddy system.
*/

	void memory_size(unsigned int memory_in_kb) {
		node *root_node = insert_node(NULL, memory_in_kb);
		if(root_node != NULL) {
			is_memorysize_command_used = TRUE;
		}
		printf("\nROOT NODE DATA: %u ", root_node->node_size);
		//return root_node;
	}

/* 
	Function to checks whether the parent node's children are free and merge the nodes of the buddy system binary tree 
	parent_node - the parent node of that freed node. 
	Returns TRUE if the child nodes are merged i.e. child nodes got deleted
*/

	int merge_nodes(node *parent_node) {
		node *left_child = parent_node->left;
		node *right_child = parent_node->right;
		if ((!(left_child->is_being_used)) && (!(right_child->is_being_used))) {
			parent_node->left = NULL;
			parent_node->right = NULL;
			parent_node->is_being_used = FALSE;
			free(left_child);
			free(right_child);
			return TRUE;
		}
		return FALSE;
	}

	/*
		Function to mark the node as not being used.
		This function will specially be used in Slab allocator where the node which is not being used is not supposed 
		to be deleted as the Slab tree has to be pre splitted to be efficient.
	*/

	int mark_as_not_used(node *some_node) {
		some_node->is_being_used = FALSE;
		return FALSE;
	}

	/*
		Function to create the tree for the slab allocator.
		This tree is pre-splitted. Thus, making the slab allocation more efficient than the buddy system.
		Returns the root node.
	*/

	// node* create_slab_tree(node *root_node) {

	// }



